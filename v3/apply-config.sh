#!/bin/bash
# Pull in the helper functions for configuring BigBlueButton
source /etc/bigbluebutton/bbb-conf/apply-lib.sh

# Variablen
COTURN_URL="ihre.coturndomain.de"
COTURN_PORT="443"
COTURN_PASSWORD="Ihr-Coturn-Passwort"

enableUFWRules

clear

echo " » Copy default.pdf and favicon.icon"
cp /home/<IhrBenutzer>/bbb/default.pdf /var/www/bigbluebutton-default/assets/default.pdf
cp /home/<IhrBenutzer>/bbb/favicon.ico /var/www/bigbluebutton-default/assets/favicon.ico

echo " » Modify logging behaviour"
sed -i 's/appLogLevel=.*/appLogLevel=Error/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/endWhenNoModerator=.*/endWhenNoModerator=true/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/endWhenNoModeratorDelayInMinutes=.*/endWhenNoModeratorDelayInMinutes=2/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/disabledFeatures=.*/disabledFeatures=learningDashboard, learningDashboardDownloadSessionData/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/level:.*/level: error/g' /usr/local/bigbluebutton/bbb-webrtc-sfu/config/default.yml
sed -i 's/loglevel =.*/loglevel = "OFF"/g' /etc/bbb-fsesl-akka/application.conf
sed -i 's/stdout-loglevel =.*/stdout-loglevel = "OFF"/g' /etc/bbb-fsesl-akka/application.conf

echo " » Enabling ClamAV"
grep -qxF "scanUploadedPresentationFiles=true" /etc/bigbluebutton/bbb-web.properties || echo "scanUploadedPresentationFiles=true" >> /etc/bigbluebutton/bbb-web.properties

echo " » Disabling Analyzing"
sed -i 's/#disabledFeatures=.*/disabledFeatures=learningDashboard/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
grep -qxF "endWhenNoModerator=true" /etc/bigbluebutton/bbb-web.properties || echo "endWhenNoModerator=true" >> /etc/bigbluebutton/bbb-web.properties
grep -qxF "endWhenNoModeratorDelayInMinutes=5" /etc/bigbluebutton/bbb-web.properties || echo "endWhenNoModeratorDelayInMinutes=5" >> /etc/bigbluebutton/bbb-web.properties
grep -qxF "muteOnStart=true" /etc/bigbluebutton/bbb-web.properties || echo "muteOnStart=true" >> /etc/bigbluebutton/bbb-web.properties

echo " » Room information"
sed -i 's/defaultWelcomeMessage=.*/defaultWelcomeMessage=\<b\>\%\%CONFNAME\%\%\<\/b\>/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/defaultWelcomeMessageFooter=.*/defaultWelcomeMessageFooter=Datenschutzerkl\&auml\;rung: \<a href\=\"https\:\/\/www\.c-rieger\.de\/datenschutz-carsten-rieger-it-services\/\" target\=\"\_blank\"\>hier\<\/a\>\./' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties

echo " » Guestroom"
sed -i 's/"app.guest.windowTitle":.*/"app.guest.windowTitle": "c-rieger.de » Guest Lobby",/g' /usr/share/bigbluebutton/html5-client/locales/en.json
sed -i 's/"app.guest.windowTitle":.*/"app.guest.windowTitle": "c-rieger.de » Wartebereich",/g' /usr/share/bigbluebutton/html5-client/locales/de.json

echo " » Design Mods"
sed -i 's/startClosed:.*/startClosed: true/' /usr/share/bigbluebutton/html5-client/private/config/settings.yml
sed -i 's/forceRelayOnFirefox:.*/forceRelayOnFirefox: true/' /usr/share/bigbluebutton/html5-client/private/config/settings.yml
sed -i 's/hidePresentationOnJoin:.*/hidePresentationOnJoin: false/' /usr/share/bigbluebutton/html5-client/private/config/settings.yml

echo " » TURNSERVER-Setting will be applied"
cat <<HERE > /usr/share/bbb-web/WEB-INF/classes/spring/turn-stun-servers.xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans-2.5.xsd">
    <bean id="stun0" class="org.bigbluebutton.web.services.turn.StunServer">
	<constructor-arg index="0" value="stun:$COTURN_URL"/>
    </bean>
    <bean id="turn0" class="org.bigbluebutton.web.services.turn.TurnServer">
        <constructor-arg index="0" value="$COTURN_PASSWORD"/>
        <constructor-arg index="1" value="turns:$COTURN_URL:$COTURN_PORT"?transport=tcp"/
        <constructor-arg index="2" value="86400"/>
    </bean>
    <bean id="stunTurnService"
            class="org.bigbluebutton.web.services.turn.StunTurnService">
        <property name="stunServers">
            <set>
                <ref bean="stun0"/>
            </set>
        </property>
        <property name="turnServers">
            <set>
                <ref bean="turn0"/>
            </set>
        </property>
    </bean>
</beans>
HERE
echo ""
echo "(c) Carsten Rieger IT-Services"
exit 0