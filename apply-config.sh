#!/bin/bash
# v. 2.5.14
/usr/bin/clear
echo ""
echo "...::: BigBlueButton :::..."
echo ""
source /etc/bigbluebutton/bbb-conf/apply-lib.sh

# Variablen 
COTURN_URL="coturn.ihredomain.de"
COTURN_PORT="443"
COTURN_PASSWORD="DasCoturn-Secret!"

# enableUFWRules
enableMultipleKurentos
echo ""

echo "  - Logging"
sed -i 's/appLogLevel=.*/appLogLevel=Error/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/level:.*/level: error/g' /usr/local/bigbluebutton/bbb-webrtc-sfu/config/default.yml
sed -i 's/loglevel =.*/loglevel = "OFF"/g' /etc/bbb-fsesl-akka/application.conf
sed -i 's/stdout-loglevel =.*/stdout-loglevel = "OFF"/g' /etc/bbb-fsesl-akka/application.conf
echo ""

echo "  - Analyzing off"
sed -i 's/#disabledFeatures=.*/disabledFeatures=learningDashboard/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
grep -qxF "learningDashboardEnabled=false" /etc/bigbluebutton/bbb-web.properties || echo "learningDashboardEnabled=false" >> /etc/bigbluebutton/bbb-web.properties
echo ""

echo "  - Disable recordings"
sed -i 's/disableRecordingDefault=.*/disableRecordingDefault=true/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/breakoutRoomsRecord=.*/breakoutRoomsRecord=false/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/allowStartStopRecording=.*/allowStartStopRecording=false/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/autoStartRecording=.*/autoStartRecording=false/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
echo ""

echo "  - DefaultMessages"
sed -i 's/defaultWelcomeMessage=.*/defaultWelcomeMessage=Konferenz: \<b\>\%\%CONFNAME\%\%\<\/b\>/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/defaultWelcomeMessageFooter=.*/defaultWelcomeMessageFooter=Die Datenschutzerkl\&auml\;rung finden Sie \<a href\=\"https\:\/\/www\.ihredomain\.de\/dsgvo\" target\=\"\_blank\"\>hier\<\/a\>\./' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
echo ""

echo "  - Set default logout URL"
sed -i 's/bigbluebutton.web.logoutURL=.*/bigbluebutton.web.logoutURL=https:\/\/www\.c\-rieger\.de/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
echo ""

echo "  - SIP-Setting"
sed -i s/sipjsHackViaWs: .*/sipjsHackViaWs: true/' /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml
echo ""

echo "  - Setting camera defaults"
#yq w -i $HTML5_CONFIG 'public.kurento.cameraProfiles.(id==low).bitrate' 50
#yq w -i $HTML5_CONFIG 'public.kurento.cameraProfiles.(id==medium).bitrate' 100
#yq w -i $HTML5_CONFIG 'public.kurento.cameraProfiles.(id==high).bitrate' 200
#yq w -i $HTML5_CONFIG 'public.kurento.cameraProfiles.(id==hd).bitrate' 300
yq w -i $HTML5_CONFIG 'public.kurento.cameraProfiles.(id==low).default' false
yq w -i $HTML5_CONFIG 'public.kurento.cameraProfiles.(id==medium).default' false
yq w -i $HTML5_CONFIG 'public.kurento.cameraProfiles.(id==high).default' true
yq w -i $HTML5_CONFIG 'public.kurento.cameraProfiles.(id==hd).default' false
echo ""

echo "  - Set guest app window title"
sed -i 's/"app.guest.windowTitle":.*/"app.guest.windowTitle": "Your Name - Guest Lobby",/g' /usr/share/meteor/bundle/programs/web.browser/app/locales/en.json
sed -i 's/"app.guest.windowTitle":.*/"app.guest.windowTitle": "Ihr Name - Wartebereich",/g' /usr/share/meteor/bundle/programs/web.browser/app/locales/de.json
echo ""

echo "  - Set default presentations"
cp /var/www/bigbluebutton-default/default.pdf /var/www/bigbluebutton-default/default.pdf.bak
cp /home/<user>/default.pdf /var/www/bigbluebutton-default/default.pdf
cp /var/www/bigbluebutton-default/default.pptx /var/www/bigbluebutton-default/default.pptx.bak
cp /home/<user>/default.pptx /var/www/bigbluebutton-default/default.pptx
cp /var/www/bigbluebutton-default/favicon.ico /var/www/bigbluebutton-default/favicon.ico.bak
cp /home/<user>/favicon.ico /var/www/bigbluebutton-default/favicon.ico
echo ""

echo "  - Custom settings"
sed -i 's/startClosed:.*/startClosed: true/' /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml
sed -i 's/hidePresentation:.*/hidePresentation: false/' /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml
echo ""

chown meteor:meteor $HTML5_CONFIG

echo "  - Update TURN server configuration turn-stun-servers.xml"
  cat <<HERE > /usr/share/bbb-web/WEB-INF/classes/spring/turn-stun-servers.xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans-2.5.xsd">

    <bean id="stun0" class="org.bigbluebutton.web.services.turn.StunServer">
        <constructor-arg index="0" value="stun:$COTURN_URL:$COTURN_PORT"/>
    </bean>


    <bean id="turn0" class="org.bigbluebutton.web.services.turn.TurnServer">
        <constructor-arg index="0" value="$COTURN_PASSWORD"/>
        <constructor-arg index="1" value="turns:$COTURN_URL:$COTURN_PORT?transport=tcp"/>
        <constructor-arg index="2" value="86400"/>
    </bean>

    <bean id="turn1" class="org.bigbluebutton.web.services.turn.TurnServer">
        <constructor-arg index="0" value="$COTURN_PASSWORD"/>
        <constructor-arg index="1" value="turn:$COTURN_URL:$COTURN_PORT?transport=tcp"/>
        <constructor-arg index="2" value="86400"/>
    </bean>

    <bean id="stunTurnService"
            class="org.bigbluebutton.web.services.turn.StunTurnService">
        <property name="stunServers">
            <set>
                <ref bean="stun0"/>
            </set>
        </property>
        <property name="turnServers">
            <set>
                <ref bean="turn0"/>
                <ref bean="turn1"/>
            </set>
        </property>
    </bean>
</beans>
HERE
echo ""
echo "(c) Carsten Rieger IT-Services"
