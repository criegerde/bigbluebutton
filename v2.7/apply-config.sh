#!/bin/bash

# Pull in the helper functions for configuring BigBlueButton
source /etc/bigbluebutton/bbb-conf/apply-lib.sh
TARGET=/usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml

# enableUFWRules

# Variablen 
COTURN_URL="coturn.ihredomain.de"
COTURN_PORT="443"
COTURN_PASSWORD="Ihr-C0turn.Secret"

enableMultipleKurentos
echo ""

echo "  - Logging"
sed -i 's/appLogLevel=.*/appLogLevel=Error/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/level:.*/level: error/g' /usr/local/bigbluebutton/bbb-webrtc-sfu/config/default.yml
sed -i 's/loglevel =.*/loglevel = "OFF"/g' /etc/bbb-fsesl-akka/application.conf
sed -i 's/stdout-loglevel =.*/stdout-loglevel = "OFF"/g' /etc/bbb-fsesl-akka/application.conf
echo ""

echo "  - Analyzing off"
sed -i 's/#disabledFeatures=.*/disabledFeatures=learningDashboard/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
grep -qxF "learningDashboardEnabled=false" /etc/bigbluebutton/bbb-web.properties || echo "learningDashboardEnabled=false" >> /etc/bigbluebutton/bbb-web.properties
grep -qxF "endWhenNoModerator=true" /etc/bigbluebutton/bbb-web.properties || echo "endWhenNoModerator=true" >> /etc/bigbluebutton/bbb-web.properties
grep -qxF "endWhenNoModeratorDelayInMinutes=5" /etc/bigbluebutton/bbb-web.properties || echo "endWhenNoModeratorDelayInMinutes=5" >> /etc/bigbluebutton/bbb-web.properties
grep -qxF "muteOnStart=true" /etc/bigbluebutton/bbb-web.properties || echo "muteOnStart=true" >> /etc/bigbluebutton/bbb-web.properties
echo ""

#echo "  - Enable recordings"
#sed -i 's/disableRecordingDefault=.*/disableRecordingDefault=false/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
#sed -i 's/breakoutRoomsRecord=.*/breakoutRoomsRecord=true/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
#sed -i 's/allowStartStopRecording=.*/allowStartStopRecording=true/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
#sed -i 's/autoStartRecording=.*/autoStartRecording=false/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
#echo ""

echo "  - Disable recordings"
sed -i 's/disableRecordingDefault=.*/disableRecordingDefault=true/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/breakoutRoomsRecord=.*/breakoutRoomsRecord=false/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/allowStartStopRecording=.*/allowStartStopRecording=false/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/autoStartRecording=.*/autoStartRecording=false/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
echo ""

echo "  - SIP-Setting"
sed -i "s/sipjsHackViaWs: .*/sipjsHackViaWs: true/" /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml
echo ""

echo "  - Room information"
sed -i 's/defaultWelcomeMessage=.*/defaultWelcomeMessage=Vorname Nachname | Firma: \<b\>\%\%CONFNAME\%\%\<\/b\>/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/defaultWelcomeMessageFooter=.*/defaultWelcomeMessageFooter=Datenschutzerkl\&auml\;rung: \<a href\=\"https\:\/\/www\.ihredomain\.de\/dsgvo\" target\=\"\_blank\"\>hier\<\/a\>\./' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
echo ""

echo "  - Guestroom"
sed -i 's/"app.guest.windowTitle":.*/"app.guest.windowTitle": "Your Name - Guest Lobby",/g' /usr/share/meteor/bundle/programs/web.browser/app/locales/en.json
sed -i 's/"app.guest.windowTitle":.*/"app.guest.windowTitle": "Ihr Name - Wartebereich",/g' /usr/share/meteor/bundle/programs/web.browser/app/locales/de.json
echo ""

echo "  - Default PDF/favicon.ico"
cp ~/bbb/default.pdf /var/www/bigbluebutton-default/assets/default.pdf
cp ~/bbb/favicon.ico /var/www/bigbluebutton-default/assets/favicon.ico
echo ""

echo "  - Weblayout"
yq w -i $TARGET 'public.layout.hidePresentationOnJoin' false
yq w -i $TARGET 'public.layout.showParticipantsOnLogin' true
yq w -i $TARGET 'public.media.forceRelay' true
echo ""

echo "  - Tabtitle"
yq w -i $TARGET 'public.app.clientTitle' "Ihr Titel"
yq w -i $TARGET 'public.app.helpLink' "https://www.IhreWunschURL.de/"
echo ""

echo "  - Webcam"
yq w -i $TARGET 'public.kurento.cameraProfiles.(id==low).bitrate' 100
yq w -i $TARGET 'public.kurento.cameraProfiles.(id==medium).bitrate' 200
yq w -i $TARGET 'public.kurento.cameraProfiles.(id==high).bitrate' 600
yq w -i $TARGET 'public.kurento.cameraProfiles.(id==hd).bitrate' 800
yq w -i $TARGET 'public.kurento.cameraProfiles.(id==low).default' false
yq w -i $TARGET 'public.kurento.cameraProfiles.(id==medium).default' false
yq w -i $TARGET 'public.kurento.cameraProfiles.(id==high).default' true
yq w -i $TARGET 'public.kurento.cameraProfiles.(id==hd).default' false
echo ""

echo "  - Screensharing"
yq w -i $TARGET 'public.kurento.screenshare.constraints.audio' true
yq w -i $TARGET 'public.kurento.screenshare.constraints.bitrate' 2000
yq w -i $TARGET 'public.kurento.screenshare.constraints.video.frameRate.ideal' 10
yq w -i $TARGET 'public.kurento.screenshare.constraints.video.frameRate.max' 20
yq w -i $TARGET 'public.kurento.screenshare.constraints.video.width.max' 1920
yq w -i $TARGET 'public.kurento.screenshare.constraints.video.height.max' 1080
echo ""

echo "  - Noiselevel"
sed -i 's/<param name="comfort-noise" value="1400"\/>/<param name="comfort-noise" value="0"\/>/g' /opt/freeswitch/conf/autoload_configs/conference.conf.xml
echo ""

chown meteor:meteor $TARGET
chown meteor:meteor $HTML5_CONFIG

echo "  - Update TURN server configuration turn-stun-servers.xml"
cat <<HERE > /usr/share/bbb-web/WEB-INF/classes/spring/turn-stun-servers.xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans-2.5.xsd">
    <bean id="stun0" class="org.bigbluebutton.web.services.turn.StunServer">
        <constructor-arg index="0" value="stun:$COTURN_URL"/>
    </bean>

    <bean id="turn0" class="org.bigbluebutton.web.services.turn.TurnServer">
        <constructor-arg index="0" value="$COTURN_PASSWORD"/>
        <constructor-arg index="1" value="turns:$COTURN_URL:$COTURN_PORT"?transport=tcp"/>
        <constructor-arg index="2" value="86400"/>
    </bean>

    <bean id="turn1" class="org.bigbluebutton.web.services.turn.TurnServer">
        <constructor-arg index="0" value="$COTURN_PASSWORD"/>
        <constructor-arg index="1" value="turns:$COTURN_URL:$COTURN_PORT"?transport=tcp"/>
        <constructor-arg index="2" value="86400"/>
    </bean>
    <bean id="stunTurnService"
            class="org.bigbluebutton.web.services.turn.StunTurnService">
        <property name="stunServers">
            <set>
                <ref bean="stun0"/>
            </set>
        </property>
        <property name="turnServers">
            <set>
                <ref bean="turn0"/>
                <ref bean="turn1"/>
            </set>
        </property>
    </bean>
</beans>
HERE
echo ""
echo "(c) Carsten Rieger IT-Services"
exit 0