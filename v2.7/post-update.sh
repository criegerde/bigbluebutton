#!/bin/bash
#################################
# Restore der BBB-Konfiguration #
#################################
clear
echo " » Kopiere die 'apply-config.sh' ..."
cp ~/bbb/apply-config.sh /etc/bigbluebutton/bbb-conf/
echo " » Kopiere die 'default.pdf' ..."
cp ~/bbb/default.pdf /var/www/bigbluebutton-default/assets/
echo " » Kopiere die 'conference.conf.xml' ..."
cp ~/bbb/conference.conf.xml /opt/freeswitch/conf/autoload_configs/
echo " » Kopiere die 'settings.yml' ..."
cp ~/bbb/settings.yml /usr/share/meteor/bundle/programs/server/assets/app/config/
echo " » Kopiere die 'sip.nginx' ..."
cp ~/bbb/sip.nginx /usr/share/bigbluebutton/nginx/
echo " » Kopiere Hintergrundbilder ..."
cp -ar ~/bbb/virtual-backgrounds /usr/share/meteor/bundle/programs/web.browser/app/resources/images/
cp  ~/bbb/bbb-html5.yml /etc/bigbluebutton/
echo ""
exit 0
# (c) c-rieger.de