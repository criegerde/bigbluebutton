#!/bin/bash
mkdir -p ~/bbb/
echo " » Sichere die 'apply-config.sh' ..."
cp /etc/bigbluebutton/bbb-conf/apply-config.sh ~/bbb/
echo " » Sichere die 'default.pdf' ..."
cp /var/www/bigbluebutton-default/assets/default.pdf ~/bbb/
echo " » Sichere die 'conference.conf.xml' ..."
cp /opt/freeswitch/conf/autoload_configs/conference.conf.xml ~/bbb/
echo " » Sichere die 'settings.yml' ..."
cp /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml ~/bbb/
echo " » Sichere die 'sip.nginx' ..."
cp /usr/share/bigbluebutton/nginx/sip.nginx ~/bbb/
echo " » Sichere Hintergrundbilder ..."
cp -ar /usr/share/meteor/bundle/programs/web.browser/app/resources/images/virtual-backgrounds ~/bbb/
cp /etc/bigbluebutton/bbb-html5.yml ~/bbb/
bbb-conf --stop
docker ps -q -all | xargs docker stop
docker ps -q -all | xargs docker rm
docker rmi --force $(docker images -a -q)
docker system prune
service docker stop
rm -rf /var/lib/docker/*
apt purge docker-ce docker-ce-cli containerd.io -y
dpkg -l | grep -w bbb |  awk '{print $2}' | xargs apt-get purge -y
dpkg -l | grep -w mongodb | awk '{print $2}' | xargs apt-get purge -y
dpkg -l | grep -w kurento | awk '{print $2}' | xargs apt-get purge -y
apt-get purge openjdk* openjdk-*-jre kms-* -y
apt-get autoremove -y
apt-get clean
apt-get purge ruby rubygems -y
rm /var/lib/gems/2.* -rf
update-rc.d nginx remove
apt purge nginx nginx-common -y
apt purge redis-server -y
rm -rf /var/lib/redis 
rm -rf /var/log/redis
cd /etc/apt/sources.list.d/
ls | grep bigbluebutton | xargs rm
ls | grep mongodb | xargs rm
ls | grep node | xargs rm
ls | grep rmescandon | xargs rm
ls | grep libreoffice | xargs rm
cd /etc/apt/trusted.gpg.d/
ls | grep bigbluebutton | xargs rm
ls | grep rmescandon | xargs rm
ls | grep libreoffice | xargs rm
sed -i 's/^deb \[arch\=amd64\] https\:\/\/download\.docker\.com\/linux\/ubuntu bionic stable$/\#deb \[arch\=amd64\] https\:\/\/download\.docker\.com\/linux\/ubuntu bionic stable/g' /etc/apt/sources.list
cd /var/lib/apt/lists/ && ls | grep bigbluebutton | xargs rm
cd /var/lib/apt/lists/ && ls | grep libreoffice | xargs rm
cd /var/lib/apt/lists/ && ls | grep certbot | xargs rm
cd /var/lib/apt/lists/ && ls | grep mongodb | xargs rm
cd /var/lib/apt/lists/ && ls | grep docker | xargs rm
apt-key del $(apt-key list | awk 'NR=='`expr $(apt-key list | grep --line-number --regexp "BigBlueButton" | cut --fields 1 --delimiter ":") - 1`'{print;exit}' | sed -e 's/ //g')
apt-key del $(apt-key list | awk 'NR=='`expr $(apt-key list | grep --line-number --regexp "Kurento" | cut --fields 1 --delimiter ":") - 1`'{print;exit}' | sed -e 's/ //g')
apt-key del $(apt-key list | awk 'NR=='`expr $(apt-key list | grep --line-number --regexp "MongoDB" | cut --fields 1 --delimiter ":") - 1`'{print;exit}' | sed -e 's/ //g')
apt-key del $(apt-key list | awk 'NR=='`expr $(apt-key list | grep --line-number --regexp "Docker" | cut --fields 1 --delimiter ":") - 1`'{print;exit}' | sed -e 's/ //g')
rm -rf /var/bigbluebutton /opt/freeswitch /usr/share/etherpad-lite /usr/local/bigbluebutton /etc/bigbluebutton  /usr/share/meteor /usr/share/bbb-libreoffice-conversion /usr/share/bbb-web /etc/systemd/system/bbb-webrtc-sfu.service.d /var/tmp/bbb-kms-last-restart.txt /var/log/bigbluebutton /var/log/kurento-media-server /var/log/bbb-apps-akka /var/log/bbb-fsesl-akka /var/log/bbb-webrtc-sfu /var/lib/kurento /var/kurento /var/log/mongodb /etc/kurento /run/bbb-fsesl-akka ./run/bbb-apps-akka /etc/systemd/system/multi-user.target.wants/bbb-web.service /etc/systemd/system/multi-user.target.wants/bbb-rap-resque-worker.service /etc/systemd/system/multi-user.target.wants/bbb-rap-starter.service ~/.bundle/cache
deluser bigbluebutton 
deluser mongodb
deluser kurento
delgroup mongodb
apt autoremove -y && apt autoclean -y
exit 0