#!/bin/bash
####################
# Update BBB 2.7.x #
##########################################################
DNS="ihre.domain.de"
##########################################################
if [ -f /tmp/bbb_updateskript ]; then
        clear
        clear
        echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++"
        echo ""
        echo " » Das Updateskript ist bereits aktiv - *ABBRUCH*"
        echo " » Oder wurde ein vorheriger Prozess abgebrochen?"
	echo ""
	echo " » "$(ls /tmp/bbb_updateskript)
	echo ""
        echo " » Entfernen Sie ggf. die Datei mit diesem Befehl:"
        echo " » sudo rm -f /tmp/bbb_updateskript"     
        echo ""
        echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++"
        echo ""
        exit 1
fi
touch /tmp/bbb_updateskript
apt-mark unhold bbb-*
apt update
apt upgrade -V
apt-mark hold bbb-*
apt autoremove -y
apt autoclean
clear
echo -n " » BBB Neustart gewünscht [y|n]?"
read answer
echo " » Kopiere die 'apply-config.sh' ..."
cp ~/bbb/apply-config.sh /etc/bigbluebutton/bbb-conf/
if [ "$answer" != "${answer#[YyjJ]}" ] ;then
	sleep 2
	echo ""
	bbb-conf --setip $DNS
fi
        echo ""
        echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo ""
if [ -e /var/run/reboot-required ]; then
        echo -e " »\e[1;31m ACHTUNG: ES IST EIN SERVERNEUSTART ERFORDERLICH.\033[0m"
        echo ""
        echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
else
        echo -e " »\033[32m  KEIN Serverneustart notwendig.\033[0m"
        echo ""
        echo " ++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
fi
echo ""
rm -f /tmp/bbb_updateskript
exit 0
# (c) Carsten Rieger, https://www.c-rieger.de