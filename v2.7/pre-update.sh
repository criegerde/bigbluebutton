#!/bin/bash
################################
# Backup der BBB-Konfiguration #
################################
clear
mkdir -p ~/bbb
echo " » Sichere die 'apply-config.sh' ..."
cp /etc/bigbluebutton/bbb-conf/apply-config.sh ~/bbb/
echo " » Sichere die 'default.pdf' ..."
cp /var/www/bigbluebutton-default/assets/default.pdf ~/bbb/
echo " » Sichere die 'conference.conf.xml' ..."
cp /opt/freeswitch/conf/autoload_configs/conference.conf.xml ~/bbb/
echo " » Sichere die 'settings.yml' ..."
cp /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml ~/bbb/
echo " » Sichere die 'sip.nginx' ..."
cp /usr/share/bigbluebutton/nginx/sip.nginx ~/bbb/
echo " » Sichere Hintergrundbilder ..."
cp -ar /usr/share/meteor/bundle/programs/web.browser/app/resources/images/virtual-backgrounds ~/bbb/
cp /etc/bigbluebutton/bbb-html5.yml ~/bbb/
echo ""
echo " »» ~/bbb/"
echo ""
ls -lsha ~/bbb
echo ""
exit 0
# (c) c-rieger.de