#!/bin/bash

# Pull in the helper functions for configuring BigBlueButton
source /etc/bigbluebutton/bbb-conf/apply-lib.sh

# enableUFWRules

# Variablen 
COTURN_URL="coturn.ihredomain.de"
COTURN_PORT="443"
COTURN_PASSWORD="Ihr-C0turn.Secret"

enableMultipleKurentos
echo ""

echo "  - Logging"
sed -i 's/appLogLevel=.*/appLogLevel=Error/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/level:.*/level: error/g' /usr/local/bigbluebutton/bbb-webrtc-sfu/config/default.yml
sed -i 's/loglevel =.*/loglevel = "OFF"/g' /etc/bbb-fsesl-akka/application.conf
sed -i 's/stdout-loglevel =.*/stdout-loglevel = "OFF"/g' /etc/bbb-fsesl-akka/application.conf
echo ""

echo "  - Analyzing off"
sed -i 's/#disabledFeatures=.*/disabledFeatures=learningDashboard/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
grep -qxF "learningDashboardEnabled=false" /etc/bigbluebutton/bbb-web.properties || echo "learningDashboardEnabled=false" >> /etc/bigbluebutton/bbb-web.properties
grep -qxF "endWhenNoModerator=true" /etc/bigbluebutton/bbb-web.properties || echo "endWhenNoModerator=true" >> /etc/bigbluebutton/bbb-web.properties
grep -qxF "endWhenNoModeratorDelayInMinutes=5" /etc/bigbluebutton/bbb-web.properties || echo "endWhenNoModeratorDelayInMinutes=5" >> /etc/bigbluebutton/bbb-web.properties
echo ""

#echo "  - Enable recordings"
#sed -i 's/disableRecordingDefault=.*/disableRecordingDefault=false/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
#sed -i 's/breakoutRoomsRecord=.*/breakoutRoomsRecord=true/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
#sed -i 's/allowStartStopRecording=.*/allowStartStopRecording=true/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
#sed -i 's/autoStartRecording=.*/autoStartRecording=false/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
#echo ""

echo "  - Disable recordings"
sed -i 's/disableRecordingDefault=.*/disableRecordingDefault=true/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/breakoutRoomsRecord=.*/breakoutRoomsRecord=false/g' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/allowStartStopRecording=.*/allowStartStopRecording=false/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/autoStartRecording=.*/autoStartRecording=false/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
echo ""

echo "  - SIP-Setting"
sed -i "s/sipjsHackViaWs: .*/sipjsHackViaWs: true/" /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml
echo ""

echo "  - SIP-Setting"
sed -i "s/sipjsHackViaWs: .*/sipjsHackViaWs: true/" /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml
echo ""

echo "  - DefaultMessages"
sed -i 's/defaultWelcomeMessage=.*/defaultWelcomeMessage=Ihr Name: \<b\>\%\%CONFNAME\%\%\<\/b\>/' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed -i 's/defaultWelcomeMessageFooter=.*/defaultWelcomeMessageFooter=Datenschutzerkl\&auml\;rung: \<a href\=\"https\:\/\/www\.ihredomain\.de\/datenschutz\" target\=\"\_blank\"\>hier\<\/a\>\./' /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
echo ""

echo "  - Set guest app window title"
sed -i 's/"app.guest.windowTitle":.*/"app.guest.windowTitle": "Ihr Name - Guest Lobby",/g' /usr/share/meteor/bundle/programs/web.browser/app/locales/en.json
sed -i 's/"app.guest.windowTitle":.*/"app.guest.windowTitle": "Ihr Name - Wartebereich",/g' /usr/share/meteor/bundle/programs/web.browser/app/locales/de.json
echo ""

echo "  - Set default presentations"
cp /var/www/bigbluebutton-default/assets/default.pdf /var/www/bigbluebutton-default/assets/default.pdf.bak
cp /home/<Benutzer>/default.pdf /var/www/bigbluebutton-default/assets/default.pdf
cp /var/www/bigbluebutton-default/assets/default.pptx /var/www/bigbluebutton-default/assets/default.pptx.bak
cp /home/<Benutzer>/default.pptx /var/www/bigbluebutton-default/assets/default.pptx
cp /var/www/bigbluebutton-default/assets/favicon.ico /var/www/bigbluebutton-default/assets/favicon.ico.bak
cp /home/<Benutzer>/favicon.ico /var/www/bigbluebutton-default/assets/favicon.ico
echo ""

echo "  - Custom settings"
sed -i 's/startClosed:.*/startClosed: true/' /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml
sed -i 's/hidePresentation:.*/hidePresentation: true/' /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml
echo ""

chown meteor:meteor $HTML5_CONFIG

echo "  - Update TURN server configuration turn-stun-servers.xml"
  cat <<HERE > /usr/share/bbb-web/WEB-INF/classes/spring/turn-stun-servers.xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans-2.5.xsd">
    <bean id="stun0" class="org.bigbluebutton.web.services.turn.StunServer">
        <constructor-arg index="0" value="stun:$COTURN_URL"/>
    </bean>

    <bean id="turn0" class="org.bigbluebutton.web.services.turn.TurnServer">
        <constructor-arg index="0" value="$COTURN_PASSWORD"/>
        <constructor-arg index="1" value="turns:$COTURN_URL:$COTURN_PORT"?transport=tcp"/>
        <constructor-arg index="2" value="86400"/>
    </bean>

    <bean id="turn1" class="org.bigbluebutton.web.services.turn.TurnServer">
        <constructor-arg index="0" value="$COTURN_PASSWORD"/>
        <constructor-arg index="1" value="turns:$COTURN_URL:$COTURN_PORT"?transport=tcp"/>
        <constructor-arg index="2" value="86400"/>
    </bean>
    <bean id="stunTurnService"
            class="org.bigbluebutton.web.services.turn.StunTurnService">
        <property name="stunServers">
            <set>
                <ref bean="stun0"/>
            </set>
        </property>
        <property name="turnServers">
            <set>
                <ref bean="turn0"/>
                <ref bean="turn1"/>
            </set>
        </property>
    </bean>
</beans>
HERE
echo ""
echo "(c) Carsten Rieger IT-Services"
exit 0
